﻿/// <reference path="../default.aspx" />

var url_page = window.location.pathname;

function Login() {
   
 
    var usuario = $("#usuario").val();
    var clave = $("#clave").val();
    var item = {
        "usuario": $("#usuario").val(),
        "clave": $("#clave").val()
    }
   
    $.ajax({ 
        type: "POST",
        url: url_page + '/Login',
        data: $.toJSON({
            user: usuario,
            pass:clave
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
          
            var data = $.parseJSON(response.d);
            if (data) {
              
                window.location.href = 'default.aspx';
            }
            else {
                alert("Usuario o Clave Incorrectos")
            }
        },
        error: function (response) {
            alert(response.responseText);
         
         
        }
    });
    return false;
}

function LogOff()
{
   
    $.ajax({
        type: "POST",
        url: '/solicitudequipos/Login.aspx/LogOff',        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: $.toJSON({}),
        success: function (response) {
       
            var data = $.parseJSON(response.d);

            if (data) {
                          
                window.location.replace(url_page)

            }

        },
        error: function (response) {
            alert(response.responseText);


        }
    });
    return false;
}

