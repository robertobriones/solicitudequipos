﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.master" CodeFile="default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Header" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Contenido" runat="server">

    <div class="row">
      
        <div class="input-field col s12 offset-s11">
            <p>Bienvenido </p>
            <% Util.session_user.user.ToString() %>
            <% If (HttpContext.Current.Session("usuario") IsNot Nothing) Then
            %><a class="waves-effect waves-light btn" onclick="LogOff();"><i class="material-icons left">power_settings_new</i>Salir</a> 
            <% 
            End If%>
        </div>
         
    </div>
  

           


    <div class="row">
        <div class="input-field col s3 offset-s3">
            <input id="txtSolicitud" type="text" />
            <label for="txtSolicitud">N° Solicitud</label>
        </div>
        <div class="input-field col s3">
            <a class="waves-effect waves-light btn"><i class="material-icons left">search</i>Buscar</a>
        </div>
    </div>

    <table class="highlight">
        <thead>
            <tr>

                <th data-field="id">Solicitud</th>
                <th data-field="id">Rut</th>
                <th data-field="name">Nombre</th>
                <th data-field="price">Sucursal</th>
                <th data-field="price">Cargo</th>
                <th data-field="price">Equipo</th>
                <th data-field="price">Tipo Equipo</th>
                <th data-field="price">Detalle</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>1</td>
                <td>16514103-2</td>
                <td>ROBERTO ANTONIO BRIONES DELGADO</td>
                <td>ADMINISTRACION</td>
                <td>SOPORTE INFORMATICO</td>
                <td>LENOVO</td>
                <td>CREDITO Y COBRANZA</td>
                <td><a class="btn-floating waves-effect waves-light red" onclick="loadModalSolicitud();"><i class="material-icons">details</i></a></td>
            </tr>
            <tr>
                <td>2</td>
                <td>16514103-2</td>
                <td>ROBERTO ANTONIO BRIONES DELGADO</td>
                <td>ADMINISTRACION</td>
                <td>SOPORTE INFORMATICO</td>
                <td>HP</td>
                <td>TI</td>
                <td><a class="btn-floating waves-effect waves-light red" onclick="loadModalSolicitud();"><i class="material-icons">details</i></a></td>
            </tr>
            <tr>
                <td>3</td>
                <td>16514103-2</td>
                <td>ROBERTO ANTONIO BRIONES DELGADO</td>
                <td>ADMINISTRACION</td>
                <td>SOPORTE INFORMATICO</td>
                <td>HP</td>
                <td>TI</td>
                <td><a class="btn-floating waves-effect waves-light red" onclick="loadModalSolicitud();"><i class="material-icons">details</i></a></td>
            </tr>
        </tbody>
    </table>


    <div class="row">
        <div class="input-field col offset-s11">
            <a class="btn-floating btn-large waves-effect waves-light #01579b light-blue darken-4" onclick="loadModalSolicitud();"><i class="material-icons">add</i></a>
        </div>
    </div>




    <%--Modal Popup Nueva Solicitud--%>


    <!-- Modal Trigger -->

    <!-- Modal Structure -->
    <div id="modal-solicitud" class="modal">
        <div class="modal-content">
            <div class="row">
                <div class="input-field col s12; center-align">
                    <h4>Solicitud </h4>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="txtNombre" type="text" class="validate"/>
                    <label for="txtNombre">Nombre</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="txtRut" type="text" class="validate"/>
                    <label for="txtRut">Rut</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <select id="cmbEmpresa">
                        <option value="" disabled="disabled">Seleccione la Empresa</option>
                        <option value="1">Automotriz Cordillera S.A.</option>
                        <option value="2">CECOR S.A.</option>
                        <option value="3">Administradora Cordillera S.A.</option>
                    </select>
                    <label>Empresa</label>
                </div>
                <div class="input-field col s6">
                    <select id="cmbSucursal">
                        <option value="" disabled="disabled" selected="selected">Seleccione la Sucursal</option>
                        <option value="1">Paicavi Kia-nvo</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                    <label>Sucursal</label>
                </div>


            </div>
            <div class="row">

                <div class="input-field col s6">
                    <select id="cmbCargo" onchange="setCmbEquipo()">
                        <option value="" disabled="disabled" selected="selected">Seleccione El Cargo</option>
                        <option value="1">Jefe Ventas</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                    <label>Cargo</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="txtEquipo" type="text" disabled="disabled" placeholder="Equipo">
                    <label for="txtEquipo">Equipo</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="txtTeq" type="text" disabled="disabled" placeholder="Tipo Equipo">
                    <label for="txtTeq">Tipo Equipo</label>
                </div>
                <div class="input-field col s6">
                   <select id="cmbTipoIngreso" onchange="setCmbTipoIngreso()">
                        <option value="" disabled="disabled" selected="selected">Seleccione El Cargo</option>
                        <option value="1">Nuevo Ingreso</option>
                        <option value="2">Cambio Por Deterioro</option>
                    </select>
                    <label>Tipo Ingreso</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="txtComentario" class="materialize-textarea"></textarea>
                    <label for="txtComentario">Comentarios</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">

                    <a class="waves-effect waves-light btn" onclick="closeModalSolicitud();"><i class="material-icons left">save</i>Guardar</a>
                    <a class="waves-effect waves-light btn" onclick="closeModalSolicitud();"><i class="material-icons left">delete</i>Eliminar</a>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>--%>
     <script src="js_pages/default.js"></script>
    <script src="js_pages/Login.js"></script>
</asp:Content>









