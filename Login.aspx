﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.master"  CodeFile="Login.aspx.vb" Inherits="Login" %>

<asp:Content ContentPlaceHolderID ="head" runat="server" ></asp:Content>
<asp:Content ContentPlaceHolderID ="Contenido" runat ="server" >

       <script src="js_pages/Login.js"></script>

     
    <div id="login-page" class="row">
    <div class="col s4 z-depth-4 card-panel">
      
        <div class="row">
          <div class="input-field col s12 center">
            <img src="img/Logo.png" alt="" class="circle responsive-img valign profile-image-login"/>
            <p class="center login-form-text">Iniciar Sesion</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="usuario" type="text"/>
            <label for="usuario" class="center-align">Usuario</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="clave" type="password"/>
            <label for="clave">Clave</label>
          </div>
        </div>       
        <div class="row">
          <div class="input-field col s12">
             <a class="waves-effect waves-light btn" onclick="Login();"><i class="material-icons left">login</i>INICIO</a>
          </div>
        </div>
       
       
       
    </div>
  </div>

 
</asp:Content>