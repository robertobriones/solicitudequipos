﻿Imports System.Web.Services
Imports Newtonsoft.Json

Partial Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    <WebMethod()>
    Public Shared Function Login(user As String, pass As String) As Boolean

        Dim usuario As New User()
        usuario.user = user
        usuario.clave = pass



        If (usuario.groupExists(usuario) IsNot Nothing) Then
            Util.session_user = usuario
            HttpContext.Current.Session("usuario") = Util.session_user
            Return JsonConvert.SerializeObject(True)
        Else
            Return False
        End If



    End Function
    <WebMethod()>
    Public Shared Function LogOff() As Boolean

        Util.session_user = Nothing
        HttpContext.Current.Session("usuario") = Nothing

        Return JsonConvert.SerializeObject(True)


    End Function


End Class


